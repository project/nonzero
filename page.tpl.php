<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
  <title><?php print $head_title ?></title>
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <?php print $head ?>
  <?php print $styles ?>
  <link rel="alternate stylesheet" type="text/css" title="Red" href="<?php print base_path() . path_to_theme() ?>/style_red.css" />
  <link rel="alternate stylesheet" type="text/css" title="Blue" href="<?php print base_path() . path_to_theme() ?>/style_blue.css" />
  <link rel="alternate stylesheet" type="text/css" title="Green" href="<?php print base_path() . path_to_theme() ?>/style_green.css" />
  <link rel="alternate stylesheet" type="text/css" title="Brown" href="<?php print base_path() . path_to_theme() ?>/style_brown.css" />
  <link rel="alternate stylesheet" type="text/css" title="Magenta" href="<?php print base_path() . path_to_theme() ?>/style_magenta.css" />
  <?php print $scripts ?>
</head>
<body>

<div id="header">
	<div id="header_inner" class="fluid">
		<div id="logo">
		  <?php if ($logo) : ?>
			<span><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print($logo) ?>" alt="<?php print t('Home') ?>" border="0" /></a></span>
		  <?php endif; ?>
		  <?php if ($site_name) : ?>
			<h1><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print($site_name) ?></a></h1>
		  <?php endif;?>
		  <?php if ($site_slogan) : ?>
			<h2><?php print($site_slogan) ?></h2>
		  <?php endif;?>
		</div>
		<div id="menu">			
			<?php print theme('links', $primary_links); ?>
		</div>
	</div>
</div>
<div id="main">
	<div id="main_inner" class="fluid">
	  <?php
	    $numcolumns = 1;
	    $columntype = '';
      if($sidebar_left != "") $numcolumns++;
      if($sidebar_right != "") $numcolumns++;
      $columntype = ($numcolumns == 1) ? 'columnless' : (($numcolumns == 2) ? '2columns' : '3columns');
    ?>
		<div id="primaryContent_<?php print $columntype ?>">
			<div id="columnA_<?php print $columntype ?>">
			
        <?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
        <?php if ($content_top):?><div id="content-top"><?php print $content_top; ?></div><?php endif; ?>
        
				<?php if ($title != ""): ?>
				  <?php print $breadcrumb ?>
				  <h3><?php print $title ?></h3>
				  <?php if ($tabs != ""): ?>
					 <div class="tabs"><?php print $tabs ?></div>
				  <?php endif; ?>
				<?php endif; ?>
		
        <?php print $help; ?>
        <?php print $messages; ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
        <?php if ($content_bottom): ?><div id="content-bottom"><?php print $content_bottom; ?></div><?php endif; ?>
			</div>
		</div>
		
		<?php if($numcolumns > 1) { ?>
		
		<div id="secondaryContent_<?php print $columntype ?>">
		
		  <?php if($sidebar_left != "") { ?>
			<div id="columnB_<?php print $columntype ?>">
				  <?php print $sidebar_left ?>
			</div>
			<?php } ?>
			
		  <?php if($sidebar_right != "") { ?>
			<div id="columnC_<?php print $columntype ?>">
				  <?php print $sidebar_right ?>
			</div>
			<?php } ?>
			
		</div>
		<?php } ?>
		<div class="clear-block" />
  </div>
</div>
<?php if ($footer_message) : ?>
<div id="footer" class="fluid">
    <p><?php print $footer_message;?></p>
</div>
<?php endif; ?>
<?php print $closure;?>
</body>
</html>
