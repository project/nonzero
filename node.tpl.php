<div class="post node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
  <?php if ($page == 0) { ?>
    <h3><a href="<?php print $node_url ?>"><?php print $title ?></a></h3>
  <?php } else { ?>
    <h3>&nbsp;</h3>
  <?php } ?>
  
  <?php if ($picture) print $picture; ?>  
	<ul class="post_info">
    <?php if ($submitted): ?>
					<li class="date"><span class="submitted"><?php print t('Posted by ') . theme('username', $node) . t(' on ') . format_date($node->created, 'custom', "F jS, Y"); ?></span></li>
		<?php endif; ?>
    <?php if ($links): ?>
		 <li class="comments"><?php print $links ?></li>
    <?php endif; ?>
	</ul>
	
  <?php if (count($taxonomy)): ?>
    <div class="taxonomy"><?php print t(' in ') . $terms ?></div>
  <?php endif; ?>
  
  <div class="content"><?php print $content ?></div>
</div>
