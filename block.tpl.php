<div class="<?php print "block block-$block->module" ?>" id="<?php print "block-$block->module-$block->delta"; ?>">
  <div class="title"><h4><?php
   $last = stristr($block->subject, ' ');
   $first = substr($block->subject, 0, strlen($block->subject) - strlen($last));
   print "<span>$first</span> $last";
    ?></h4></div>
  <div class="content"><?php print $block->content ?></div>
</div>
