<div class="post comment<?php if ($comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; ?>">
  <h3 class="title"><?php print $title ?></h3>
  <?php if ($picture) : ?>
    <?php print $picture ?>
  <?php endif; ?>
  
	<ul class="post_info">
    <?php if ($submitted): ?>
					<li class="date"><span class="submitted"><?php print $submitted ?></span></li>
		<?php endif; ?>
    <?php if ($links): ?>
		 <li class="comments"><?php print $links ?></li>
    <?php endif; ?>
	</ul>
  
  <div class="content"><?php print $content ?></div>
</div>
