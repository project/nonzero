Description:
------------
This is the nonzero theme. 

Authors:
--------
It was designed by the guys at Nodethirtythree and was then ported to Drupal by Iluminatia (www.iluminatia.com) as a contribution tu Drupal users.

Known Issues (we know we�ve got to fix this):
---------------------------------------------
* there is an extra space that appears when there are two unordered/ordered lists nested (such as in the admin menu).
* in the original theme, the primary links menu had the active link with a different CSS class. I don�t know how to simulate that with Drupal.
* it would be nice to port the other colour variations in the theme with the javascript included in the original design.

Feedback
--------
Please use Drupal system.